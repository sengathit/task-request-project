<?php 
    get_header(); 

    // global $wpdb;

// $posts = $wpdb->get_results("SELECT post_title,guid,{$wpdb->prefix}term_relationships.term_taxonomy_id,{$wpdb->prefix}term_relationships.object_id, {$wpdb->prefix}term_taxonomy.description FROM {$wpdb->prefix}term_relationships INNER JOIN {$wpdb->prefix}term_taxonomy ON {$wpdb->prefix}term_relationships.term_taxonomy_id = {$wpdb->prefix}term_taxonomy.term_id INNER JOIN {$wpdb->prefix}posts ON {$wpdb->prefix}term_relationships.object_id = ID ORDER BY object_id",OBJECT);
//var_dump($posts);

// $posts = new WP_Query(array('post_type' => 'etc'));
?>
<div class="container">
    <div class="row">
        <h1>ETC Tasks</h1>
    </div>
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
    <?php 
        $postTag = get_the_terms(get_the_ID(),'post_tag') ;  
        $projectManagerEmail = get_post_meta(get_the_ID(),'project_manager_email') ;
    ?>
        <div class="row">
            <div class="entry-content">
            
                <?php 
                    if($postTag[0]->status == 'open') {
                ?>
                <a class="d-inlieblock" href="<?php the_permalink() ; ?>"><?php the_title() ; ?></a> <span class="d-inline-block"><?php echo $postTag[0]->description ;?></span>
                <?php } ?>
            </div>
        </div>
    <?php endwhile; endif;?>
</div>

<?php get_footer();