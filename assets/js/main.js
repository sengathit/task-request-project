(function($){
    $(document).ready(function(){
        $('#startDate').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#dueDate').datepicker({
            uiLibrary: 'bootstrap4'
        });

        /*
         * Functionality for .popup class
         * on the post page.
         * The request more info button
         */
        $('.pmNeedsMoreInfo').on('click',function(){
            $('.popup').removeClass('invisible');
        });

        $('.cancelRequest').on('click',function(){
            $('.popup').addClass('invisible');
        });

        /* Clone file upload input.
         * Set the value to empty
         * or null.
         */
        $uploadContainer = $('.upload-container');
        $appendFile = $('.append-file');

        $('.btn-add-file').on('click',function(){
            $newFileUpload = $appendFile.clone();
            $newFileUpload.find('input').val('');
            $newFileUpload.appendTo($uploadContainer);
        });

        $cancel = $('.cancel');
        $closeTaskForm = $('.closeTaskForm');

        /*
        * Button that opens the dialogue to
        * close the task.
        */
        $('.closeTask').on('click',function(){
            $closeTaskForm.removeClass('invisible');
        });

        /*
        * CLOSE TASK DIALOGUE
        * Close the Close Task dialogue
        * and returns the screen to the
        * task page.
        * Default behavior of cancel
        * button is disabled to prevent
        * from navigating to another page.
        */
        $cancel.on('click',function(e){
            e.preventDefault();
            $closeTaskForm.addClass('invisible');
        });

        /*********************************************/
        /* Get data from teamwork api.  The code that*/
        /* calls the function to get the list is     */
        /* in functions/wordpress/tw-clients-api.php.*/
        /*********************************************/

        // Select the <select> tag the will render the data
        var where = $('.where');

        // Create an array that will store the data
        var datas = [];

        // Make an asynchronous call the wordpress file
        // that handles the request.
        $.get(
            'http://localhost:8888/tr/wp-admin/admin-ajax.php',
            {'action': 'get_data'},
            function(response){
                datas = response;

                // Data returns an array, but
                // has a integer of 0 right
                // after the array of datas.
                // This code will remove the 0
                // at the end array that is
                // appended after the closing
                // square bracket.
                datas = datas.replace(']0',']');
                console.log(JSON.parse(datas));
                datas = JSON.parse(datas);
                console.log(datas);
                for(data of datas){
                    if(data.status == 'active'){
                        where.append('<option value="' + data.name + '">' + data.name + '</option>')
                    }
                }
            }
        ).done(function(){
            console.log('success');
        }).fail(function(){
            console.log('failed');
        });
    });
})(jQuery);