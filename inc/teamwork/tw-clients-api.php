<?php
require('vendor/autoload.php');
use GuzzleHttp\Client as Guzzle;
use Rossedman\Teamwork\Client;
use Rossedman\Teamwork\Factory as Teamwork;

/*
 * Connect to etc/ces NAS server
 */
function connect_teamwork() {
    $client     = new Client(new Guzzle, 'twp_1ZYmXDQHJD5FWQfWQ77vl7kOgWkz', 'https://eightythreecreative.teamwork.com');
    $teamwork   = new Teamwork($client);

    return $teamwork;
}

/*
 * Return etc/ces clients
 */
function get_companies() {
    $getCompanies = connect_teamwork();

    $companies = $getCompanies->company()->all();

    $companyArray = array();
    foreach($companies['companies'] as $item) {
        $companyId = $item['id'];
        $companyName = $item['name'];

        $companyArray[] = array(
            'id' 			=> $companyId,
            'company_name'	=> $companyName
        );
    }
    return $companyArray;
}

/*
 * Return all etc/ces employees
 */
function get_all_employees() {
    $getEmployees = connect_teamwork();

    $allPeople = $getEmployees->people()->all();

    $peopleArray = array();
    foreach($allPeople['people'] as $item) {
        $empId = $item['id'];
        $firstName = $item['first-name'];
        $lastName = $item['last-name'];
        $emailAddress = $item['user-name'];
        $avatar = $item['avatar-url'];
        $companyTitle = $item['title'];

        // echo '<pre>'; print_r($item); echo '</pre>';

        $peopleArray[] = array(
            'id'            => $empId,
            'first_name'    => $firstName,
            'last_name'     => $lastName,
            'email_address' => $emailAddress,
            'avatar_url'    => $avatar,
            'float_data'    => array(
                'people_id'     => $empId,
                'name'          => $firstName . ' ' . $lastName,
                'email'         => $emailAddress,
                'job_title'     => $companyTitle
            )

        );
    }

    return $peopleArray;
}


/*
 * Get all projects
 */
function get_projects() {

    $getProjects = connect_teamwork();

    $projects = $getProjects->project()->all();

    $projectsArray = array();
    foreach($projects['projects'] as $item) {

        if($item['category']['name'] != '-Master Client Summaries') {

            $projectsArray[] = array(
                'id'                => $item['id'],
                'project_name'      => $item['name'],
                'company_link_id'   => $item['company']['id']
            );
        }
    }

    return $projectsArray;
}

/*
 * Get all Tasks from all projects.
 * Function expects an array of ids
 * not just one id.
 */
function get_tasks($ids) {
    $getTasks = connect_teamwork();

    $tasks = $getTasks->task()->all(['responsible-party-ids' => $ids]);

    $taskArray = array();
    foreach($tasks['todo-items'] as $item) {
        $taskId = $item['id'];
        $personResponsibleId = $item['responsible-party-id'];
        $projectName = $item['project-name'];
        $parentTask = $item['parent-task'];
        $toDoListName = $item['todo-list-name'];

        // Dates
        $startDate = $item['start-date'];
        $dueDate = $item['due-date'];

        // echo '<pre>'; print_r($item); echo '</pre>';
        if(!empty($startDate)) {
            $taskArray[] = array(
                'id'                      => $taskId,
                'project_name'            => $projectName,
                'parent_task'             => $parentTask,
                'to_do'                   => $toDoListName,
                'persons_responsible_id'  => explode(",", $personResponsibleId),
                'dates'                   => array(
                    'start_date'                => $startDate,
                    'due_date'                  => $dueDate
                )
            );
        }

    }

    return $taskArray;
}

function getTasks(){
    $results = connect_teamwork();

    echo json_encode($results->project()->all()['projects']);
}

add_action('wp_ajax_get_data','getTasks');
add_action('wp_ajax_nopriv_get_data','getTasks');


