<?php

/*
 * This file sets up the post types for CES/ETC/BOTH
 */

function create_post_types(){
    register_post_type(
        'ces',
        array(
            'labels' => array(
                'name' => __('CES Tasks'),
                'singular_name' => __('CES Task')
            ),
            'public' => true,
            'supports' => array('title','custom-fields'),
            'taxonomies' => array('category','post_tag'),
            'has_archive' => 'ces'
        )

    );

    register_post_type(
        'etc',
        array(
            'labels' => array(
                'name' => __('ETC Tasks'),
                'singular_name' => __('ETC Task')
            ),
            'public' => true,
            'supports' => array('title','custom-fields'),
            'taxonomies' => array('category','post_tag'),
            'has_archive' => 'etc'
        )

    );

    register_post_type(
        'both',
        array(
            'labels' => array(
                'name' => __('BOTH Tasks'),
                'singular_name' => __('BOTH Task')
            ),
            'public' => true,
            'supports' => array('title','custom-fields'),
            'taxonomies' => array('category','post_tag'),
            'has_archive' => 'both'
        )

    );
}

add_action('init','create_post_types');