<?php
// Load site scripts
function site_script(){
     wp_deregister_script('jquery');
     wp_deregister_script('jquery-ui-core');
    $pathToScripts = THEME_JS . '/main.js';
    $pathToCSS = THEME_CSS . '/pre/style.css';
     wp_enqueue_script('jquery',THEME_JS . '/dependencies/jquery-3.3.1.min.js',array(),'',true);
     wp_enqueue_script('bootstrap',THEME_JS . '/dependencies/bootstrap.bundle.min.js',array('jquery'),'',true);
     wp_enqueue_script('gijgo-js','https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js',array('jquery'),'',true);

    wp_enqueue_script('site_script',$pathToScripts,array('jquery'),'',true);

    wp_enqueue_style('main-style',$pathToCSS);
    wp_enqueue_style('gijgo-css','https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css');

}

add_action('wp_enqueue_scripts','site_script');

function admin_css(){
    $pathToCSS = THEME_CSS . '/pre/style.css';
    wp_enqueue_style('main-style',$pathToCSS);
}

add_action('admin_enqueue_scripts', 'admin_css');

