<?php
    /*
     * Data needs sanitation
     * trim inputs
     * validate email
     */

    function sendMail()
    {
        global $wpdb;
        $companyName = 'Eighty Three Creative';

        /*
         * This list will be submitted
         * to the etc_tr_posts table.
         */
        $title      = sanitize_text_field($_POST['title']);
        $name       = sanitize_text_field($_POST['name']);
        $type = 'ces';

        switch($_POST['category']) {
            CASE "7":
                $type = 'etc';
                break;
            CASE "1":
                $type = 'ces';
                break;
            CASE '8':
                $type = 'both';
                break;
        }



        /*
         * This list will be submitted
         * to the etc_tr_term_relationships table
         * as key/value pairs.  ACF Pro
         * is used to create the custom
         * fields in the WP editor.
         */
        $terms = [
            'category'  => (int) $_POST['category'],
            'post_tag'  => (int) $_POST['tags'],
            'priority'  => (int) $_POST['priority'],
            'status'    => 'open'
        ];

        /*
         * This array will be submitted
         * to the etc_tr_postmeta table
         * as key/value pairs.  ACF Pro
         * is used to create the custom
         * fields in the WP editor.
         */
        $where              = sanitize_textarea_field($_POST['where']);
        $startDate          = sanitize_text_field($_POST['startDate']);
        $dueDate            = sanitize_text_field($_POST['dueDate']);
        $resources          = sanitize_textarea_field($_POST['resources']);
        $additionalDetails  = sanitize_textarea_field($_POST['additionalDetails']);
        $needs              = sanitize_textarea_field($_POST['needs']);
        $tasks              = sanitize_textarea_field($_POST['tasks']);
        $upload             = $_FILES['upload'];
        $submitterEmail     = sanitize_email($_POST['submitterEmail']);
        $projectManagerEmail = sanitize_email($_POST['projectManagerEmail']);

        $postMeta = [
            'where'                 => $where,
            'start_date'            => $startDate,
            'due_date'              => $dueDate,
            'resources'             => $resources,
            'additional_details'    => $additionalDetails,
            'needs'                 => $needs,
            'tasks'                 => $tasks,
            'upload'                => $upload,
            'submitter_email'       => $submitterEmail,
            'project_manager_email' => $projectManagerEmail
        ];

        /*
         * This array will be submitted
         * to the etc_tr_posts table.
         */

        $args = [
            'post_author'   => $projectManagerEmail,
            'post_content'  => 'hey from body',
            'post_title'    => $title,
            'post_type'     => $type,
            'post_name'     => $name,
            'post_status'   => 'publish'
        ];



        $post_id = wp_insert_post($args,true);

        if($upload){
            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            require_once(ABSPATH . "wp-admin" . '/includes/file.php');
            require_once(ABSPATH . "wp-admin" . '/includes/media.php');
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            $fileArr = count($_FILES['upload']['name']);

            for($i = 0; $i < $fileArr;$i++){
                $uploaded = wp_upload_bits( $_FILES['upload']['name'][$i], null, file_get_contents( $_FILES['upload']['tmp_name'][$i] ) );

                $wp_filetype = wp_check_filetype( basename( $uploaded['file'] ), null );

                $wp_upload_dir = wp_upload_dir();

                $attachment = array(
                    'guid' => $wp_upload_dir['baseurl'] . _wp_relative_upload_path( $uploaded['file'] ),
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename( $uploaded['file'] )),
                    'post_status' => 'inherit'
                );


                $attach_id = wp_insert_attachment( $attachment, $uploaded['file'], $post_id );



                $attach_data = wp_generate_attachment_metadata( $attach_id, $uploaded['file'] );

                wp_update_attachment_metadata( $attach_id, $attach_data );

                update_post_meta( $post_id, '_thumbnail_id', $attach_id );
            }

        }


        if($post_id){


            /*
             * Insert custom fields from ACF pro
             * into the postmeta table.
             */
            foreach($postMeta as $key=>$val){
                add_post_meta($post_id,$key,$val);
            }

            /*
             * Insert custom fields from ACF pro
             * into the postmeta table.
             */
            foreach($terms as $key=>$val){
                wp_set_object_terms($post_id,$val,$key);
            }

            $taskUrl = $wpdb->get_var("SELECT guid FROM {$wpdb->prefix}posts WHERE ID = {$post_id}");

            $dataMapper = [];
            $postMetas = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$post_id}",OBJECT);
            foreach($postMetas as $postMeta){
                $dataMapper[$postMeta->meta_key] = $postMeta->meta_value;
            }

            $recipients = array(
                $projectManagerEmail,$submitterEmail
            );
            $to = implode(',', $recipients);

            $message = '<html>';
            $message .= '<body>';
            $message .= '<table width="100%" cellspacing="0" cellpadding="15" border="1" style="font-family:Arial; font-size:13px; color:#333333; letter-spacing:0.0125em; line-height:1.5; margin-bottom:20px; border:1px solid #dddddd; border-collapse:collapse; -webkit-font-smoothing:antialiased;">';
            $message .= '<tbody>';
            $message .= '<tr><td><h2>You have a project/task request. <a href="' . $taskUrl . '">' . $taskUrl . '</a></h2></td></tr>';

            $message .= '<tr><td>Title: <b>' . $title . '</b></td></tr>';

            $message .= '<tr><td><p>What do you need?<br>Use this section to include any and all details pertaining to what needs to be done.</p><p><b>' . $dataMapper['needs'] . '</b><p></td></tr>';

            $message .= '<tr><td><p>What tasks are required to complete this? And who is doing them?<br>Please include any details regarding other tasks needed to be required. For example, if we\'re creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you\'ve been given estimates or resources (i.e. specific people) for these tasks, please also include that information.</p><p><b>' . $dataMapper['tasks'] . '</b></p></td></tr>';
            $message .= '<tr><td><p>Where should this live in TW?<br>Please link the specific TeamWork Project name/task list (i.e. "ETC - New Hires"), if applicable.</p><p><b>' . $dataMapper['where'] . '</b></p></td></tr>';
            $message .= '<tr><td><p>Start Date: <b>' . $dataMapper['start_date'] . '</b></p></td></tr>';
            $message .= '<tr><td><p>Due Date: <b>' . $dataMapper['due_date'] . '<b></p></td></tr>';
            // $message .= '<tr><td><p>Files</p><p>' . $dataMapper['upload'] . '</p></td></tr>';
            $message .= '<tr><td><p>Will this task require any resources or involvement from the CES Team?<br>If so, please provide details</p><b>' . $dataMapper['resources'] . '</b></td></tr>';
            $message .= '</tbody>';
            $message .= '</table>';
            $message .= '</body>';
            $message .= '</html>';

            $headers = "Reply-To: Eighty Three Creative <{$submitterEmail}>\r\n";
            $headers .= "Return-Path: Eighty Three Creative <{$submitterEmail}>\r\n";
            $headers .= "From: Eighty Three Creative <{$submitterEmail}>" . PHP_EOL;
            $headers .= "Organization: Eighty Three Creative\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

            // $headers[] = 'From: FROM NAME <DOMAIN>';
            // $headers[] = 'MIME-Version: 1.0';
            // $headers[] = 'Content-Type: text/html; charset=UTF-8';

            mail($to, $title, $message, $headers,'-fgiveaway@cityelectricsupplymarketing.com');


            wp_redirect($taskUrl);
            exit;
        }else{
            wp_redirect(get_site_url() . '/etc-form');
            exit;
        }

    }
    add_action('admin_post_nopriv_etc', 'sendMail');
    add_action('admin_post_etc', 'sendMail');