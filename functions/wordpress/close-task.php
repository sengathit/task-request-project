<?php

    function closeTask()
    {
        $postID = (int)sanitize_text_field($_POST['postId']);

        wp_set_object_terms($postID, 'archive', 'status');

        $submitterEmail = sanitize_text_field($_POST['submitterEmail']);
        $projectManagerEmail = sanitize_text_field($_POST['projectManagerEmail']);
        $taskTitle = sanitize_text_field($_POST['taskTitle']);
        $url = sanitize_text_field($_POST['url']);


        $recipients = array(
            $submitterEmail,
            $projectManagerEmail
        );

        $to = implode(',', $recipients);

        $subject = $taskTitle;

        $message = '<html>';
        $message .= '<body>';
        $message .= '<table width="100%" cellspacing="0" cellpadding="15" border="1" style="font-family:Arial; font-size:13px; color:#333333; letter-spacing:0.0125em; line-height:1.5; margin-bottom:20px; border:1px solid #dddddd; border-collapse:collapse; -webkit-font-smoothing:antialiased;">';
        $message .= '<tbody>';
        $message .= '<tr>';
        $message .= '<td>';
        $message .= '<h2>Title: ' . $subject . '</h2>';
        $message .= '<p>Task is closed <a href="' . $url . '">' . $url . '</a></p>';
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '</tbody>';
        $message .= '</table>';
        $message .= '</body>';
        $message .= '</html>';


        // $headers .= "Reply-To: \r\n";
        // $headers = 'MIME-Version: 1.0' . "\r\n";
        // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        $headers = "Reply-To: Eighty Three Creative <{$projectManagerEmail}>\r\n";
        $headers .= "Return-Path: Eighty Three Creative <{$projectManagerEmail}>\r\n";
        $headers .= "From: Eighty Three Creative <{$projectManagerEmail}>" . PHP_EOL;
        $headers .= "Organization: Eighty Three Creative\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "X-Priority: 3\r\n";
        $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

        // $headers[] = 'From: FROM NAME <DOMAIN>';
        // $headers[] = 'MIME-Version: 1.0';
        // $headers[] = 'Content-Type: text/html; charset=UTF-8';
        //

        mail($to, $subject, $message, $headers, '-fgiveaway@cityelectricsupplymarketing.com');
        wp_redirect($url);
        exit;
    }

    add_action('admin_post_nopriv_close_task', 'closeTask');
    add_action('admin_post_close_task', 'closeTask');