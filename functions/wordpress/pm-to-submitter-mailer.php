<?php
    function requestMoreInfo(){
        try{
            $moreInfoNeeded = sanitize_text_field($_POST['moreInfoNeeded']);
            $company = 'Eighty Three Creative';
            $pmEmail = 'slavanh@eightythreecreative.com';
            $submitterEmail = sanitize_email($_POST['submitterEmail']);
            $guid = sanitize_text_field($_POST['taskGUID']);

            $recipients = array($submitterEmail,$pmEmail);
            $to = implode(', ', $recipients);

            $message = '<html><body><table width="100%" cellspacing="0" cellpadding="15" border="1" style="font-family:Arial; font-size:13px; color:#333333; letter-spacing:0.0125em; line-height:1.5; margin-bottom:20px; border:1px solid #dddddd; border-collapse:collapse; -webkit-font-smoothing:antialiased;"><tbody>';
            $message .= '<tr><td>I need more info about this task. Link to <a href="' . $guid . '">task request</a>.</td></tr>';
            $message .= '<tr><td><p>' . $moreInfoNeeded . '</p></td></tr>';
            $message .= '</tbody></table></body></html>';

            $headers = "Reply-To: {$company} <{$pmEmail}>\r\n";
            $headers .= "Return-Path: {$company} <{$pmEmail}>\r\n";
            $headers .= "From: {$company} <{$pmEmail}>" . PHP_EOL;
            $headers .= "Organization: {$company}\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

            if(mail($to, 'More info needed', $message, $headers,'-fgiveaway@cityelectricsupplymarketing.com')){
                wp_redirect(home_url());
                exit;
            }


        }catch(Exception $e){
            wp_redirect(home_url() . '/nope');
            exit;
        }

    }

    add_action('admin_post_nopriv_requestToSubmitter', 'requestMoreInfo');
    add_action('admin_post_requestToSubmitter', 'requestMoreInfo');