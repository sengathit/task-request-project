<?php
    /*
    Adding dashboard widgets here
    */

    global $wpdb;

    add_action('wp_dashboard_setup','ces_dashboard_add_widget');

    function ces_dashboard_add_widget(){
        wp_add_dashboard_widget('ces_dashboard',__('CES Open Tasks','dw'),'ces_dashboard_handler');
    }

    function ces_dashboard_handler(){
        global $wpdb;
        $query = "SELECT * From {$wpdb->prefix}posts WHERE post_type = 'ces' AND post_status = 'publish'";

        $results = $wpdb->get_results($query,OBJECT);

        function getTerms($terms,$postID){
            $termRelationships = [];
            foreach($terms as $term){
                $termRelationships[$term] = wp_get_object_terms($postID,$term);
            }

            return $termRelationships;
        }

        if($results){
            foreach($results as $result){
                $terms = ['post_tag','category','status','priority'];
                $details = getTerms($terms,$result->ID);

                if($details['priority'][0]->description == 'high'){
                    $priorityBG = 'bg-danger';
                }elseif($details['priority'][0]->description == 'medium'){
                    $priorityBG = 'bg-primary';
                }else{
                    $priorityBG = 'bg-warning';
                }

                $listItem = "<div class='py-1'>";
                $listItem .= "<a href='{$result->guid}'>{$result->post_title}</a>";
                $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['category'][0]->description}</span>";
                $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['post_tag'][0]->description}</span>";
                $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['status'][0]->description}</span>";
                $listItem .= "<span class='{$priorityBG} d-inline-block ml-1 p-1 text-white'>{$details['priority'][0]->description}</span></div>";

                echo $listItem;
            }
        }
    }

    add_action('wp_dashboard_setup','etc_dashboard_add_widget');

    function etc_dashboard_add_widget(){
        wp_add_dashboard_widget('etc_dashboard',__('ETC Open Tasks','dw'),'etc_dashboard_handler');
    }

    function etc_dashboard_handler(){
        global $wpdb;
        $query = "SELECT * From {$wpdb->prefix}posts WHERE post_type = 'etc' AND post_status = 'publish'";

        $results = $wpdb->get_results($query,OBJECT);

        function getEtcTerms($terms,$postID){
            $termRelationships = [];
            foreach($terms as $term){
                $termRelationships[$term] = wp_get_object_terms($postID,$term);
            }

            return $termRelationships;
        }

        if($results){
            foreach($results as $result){
                $terms = ['post_tag','category','status','priority'];
                $details = getEtcTerms($terms,$result->ID);

                if($details['priority'][0]->description == 'high'){
                    $priorityBG = 'bg-danger';
                }elseif($details['priority'][0]->description == 'medium'){
                    $priorityBG = 'bg-primary';
                }else{
                    $priorityBG = 'bg-warning';
                }

                if($details['status'][0]->slug == 'open'){
                    $listItem = "<div class='py-1'>";
                    $listItem .= "<a href='{$result->guid}'>{$result->post_title}</a>";
                    $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['category'][0]->description}</span>";
                    $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['post_tag'][0]->description}</span>";
                    $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['status'][0]->description}</span>";
                    $listItem .= "<span class='{$priorityBG} d-inline-block ml-1 p-1 text-white'>{$details['priority'][0]->description}</span></div>";

                    echo $listItem;
                }

            }
        }
    }

    add_action('wp_dashboard_setup','etc_dashboard_archived_widget');

    function etc_dashboard_archived_widget(){
        wp_add_dashboard_widget('etc_archive_dashboard',__('ETC Closed Tasks','dw'),'etc_dashboard_archived_handler');
    }

    function etc_dashboard_archived_handler(){
        global $wpdb;
        $query = "SELECT * From {$wpdb->prefix}posts WHERE post_type = 'etc' AND post_status = 'publish'";

        $results = $wpdb->get_results($query,OBJECT);

        function getEtcArchivedTerms($terms,$postID){
            $termRelationships = [];
            foreach($terms as $term){
                $termRelationships[$term] = wp_get_object_terms($postID,$term);
            }

            return $termRelationships;
        }

        if($results){
            foreach($results as $result){
                $terms = ['post_tag','category','status','priority'];
                $details = getEtcArchivedTerms($terms,$result->ID);

                if($details['status'][0]->slug == 'archive'){
                    $listItem = "<div class='py-1'>";
                    $listItem .= "<a href='{$result->guid}'>{$result->post_title}</a>";
                    $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['category'][0]->description}</span>";
                    $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['post_tag'][0]->description}</span>";
                    $listItem .= "<span class='bg-secondary d-inline-block ml-1 p-1 text-white'>{$details['status'][0]->description}</span></div>";

                    echo $listItem;
                }

            }
        }
    }
