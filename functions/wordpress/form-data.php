<?php

    function queryPostMeta($wpdb,$postID){
        $dataMapper = [];

        $postMetas = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$postID}",OBJECT);

        foreach($postMetas as $postMeta){
            $dataMapper[$postMeta->meta_key] = $postMeta->meta_value;
        }

        return $dataMapper;
    }

    function getTaxomony($postID){
        $terms = ['post_tag','category','status','priority'];
        $termRelationships = [];
        foreach($terms as $term){
            $termRelationships[$term] = wp_get_object_terms($postID,$term);
        }

        return $termRelationships;
    }





