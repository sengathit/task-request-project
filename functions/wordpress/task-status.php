<?php

/*
* Plugin Name: Status
* Description: A short example showing how to add a taxonomy called Status.
* Version: 1.0
*/

function add_task_status()
{
    $labels = [
        'name'              => _x('Status', 'taxonomy general name'),
        'singular_name'     => _x('Status', 'taxonomy singular name'),
        'search_items'      => __('Search Status'),
        'all_items'         => __('All Status'),
        'parent_item'       => __('Parent Course'),
        'parent_item_colon' => __('Parent Course:'),
        'edit_item'         => __('Edit Status'),
        'update_item'       => __('Update Status'),
        'add_new_item'      => __('Add New Status'),
        'new_item_name'     => __('New Status Name'),
        'menu_name'         => __('Status')
    ];
    $args = [
        'hierarchical'      => false,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'status'],
    ];
    register_taxonomy('status', ['ces','etc','both'], $args);

}
add_action('init', 'add_task_status');

function add_priority()
{
    $labels = [
        'name'              => _x('Priority', 'taxonomy general name'),
        'singular_name'     => _x('Priority', 'taxonomy singular name'),
        'search_items'      => __('Search Priority'),
        'all_items'         => __('All Priority'),
        'parent_item'       => __('Parent Course'),
        'parent_item_colon' => __('Parent Course:'),
        'edit_item'         => __('Edit Priority'),
        'update_item'       => __('Update Priority'),
        'add_new_item'      => __('Add New Priority'),
        'new_item_name'     => __('New Priority Name'),
        'menu_name'         => __('Priority')
    ];
    $args = [
        'hierarchical'      => false,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'priority'],
    ];
    register_taxonomy('priority', ['ces','etc','both'], $args);

}
add_action('init', 'add_priority');