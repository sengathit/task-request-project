<article class="pb-5" id="post-<?php the_ID(); ?>" <?php post_class(); ?>
    <header class="entry-header">
        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
    </header><!-- .entry-header -->
    <div>
        <form class="container" action="<?php echo esc_url(admin_url('admin-post.php')) ; ?>" method="post">
            <div class="row mb-3">
                <div class="col-sm-6 col-12">
                    <label for="submitterEmail">Your Email <span class="required">*</span></label>
                    <input type="email" class="validate" name="submitterEmail" placeholder="Your Email" required>
                </div>
                <div class="col-sm-6 col-12">
                    <label for="pmEmail">Send To <span class="required">*</span></label>
                    <input type="email" class="validate" name="pmEmail" placeholder="project manager's email" required>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-sm-6 col-12">
                    <label>Teams</label>
                    <select name="category">
                        <option value="" disabled selected>ETC/CES/BOTH</option>
                        <option value="7">ETC</option>
                        <option value="1">CES</option>
                        <option value="8">BOTH</option>
                    </select>
                </div>
                <div class="col-sm-6 col-12">
                    <label for="tags">Project of Task</label>
                    <select name="tags">
                        <option value="" disabled selected>Project/Task</option>
                        <option value="5">Project</option>
                        <option value="3">Task</option>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="priority">Priority</label>
                    <select name="priority">
                        <option value="" disabled selected>Priority</option>
                        <option value="11">Low</option>
                        <option value="12">Medium</option>
                        <option value="13">High</option>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="title">Title <span class="required">*</span></label>
                    <input class="validate" type="text" name="title" placeholder="Title">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <div>
                        <p for="needs">What do you need? <span class="required">*</span></p>
                        <label>Use this section to include any and all details pertaining to what needs to be done.</label>
                    </div>
                    <textarea class="w-100" name="needs"></textarea>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <div>
                        <p for="tasks">What tasks are required to complete this? And who is doing them? <span class="required">*</span></p>
                        <label>Please include any details regarding other tasks needed to be required. For example, if we're creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you've been given estimates or resources (i.e. specific people) for these tasks, please also include that information.</label>
                    </div>

                    <textarea class="w-100" name="tasks" placeholder="Your Answer"></textarea>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <div>
                        <p for="where">Where should this live in TW? <span class="required">*</span></p>
                        <label>Please link the specific TeamWork Project name/task list (i.e. "ETC - New Hires"), if applicable.</label>
                    </div>
                    <select name="where">
                        <?php
                            /*
                             * Get the list of companies from
                             * Teamwork API.  The code that
                             * defines the function is required
                             * in is tw-clients-api.php which is
                             * in the functions.php file.
                             */
                            try{
                                $companies = get_companies();
                                foreach($companies as $company){
                                    foreach($company as $key=>$val){
                                        if($key === 'company_name') echo "<option value='{$val}'>{$val}</option>";
                                    }
                                }
                            }catch(Exception $e) {
                                echo "<option disabled selected>No Clients</option>";
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="startDate">Start Date</label>
                    <input id="startDate" name="startDate" placeholder="mm/dd/yyyy" required>
                </div>
                <div class="col">
                    <label for="dueDate">Due Date</label>
                    <input id="dueDate" name="dueDate" placeholder="mm/dd/yyyy" required>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <div>
                        <p for="where">Any additional details?</p>
                        <label for="additionalDetails">Is there any extra context around this task you would like us to know?</label>
                    </div>
                    <textarea class="w-100" name="additionalDetails" placeholder="Your Answer"></textarea>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <p for="upload" class="btn">Uploads</p>
                    <input type="file" name="upload" placeholder="upload">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <div>
                        <p>Will this task require any resources or involvement from the CES Team? <span class="required">*</span>
                            If so, please provide details</p>
                        <label for="resources">If so, please provide details</label>
                    </div>
                    <textarea class="w-100" type="text" name="resources" placeholder="Your answer"></textarea>
                </div>
            </div>
            <input type="hidden" name="type" value="etc">
            <input type="hidden" name="action" value="etc">
            <input type="submit" class="btn btn-primary" value="Submit ETC Task">
        </form>
    </div>
</article><!-- #post-## -->
