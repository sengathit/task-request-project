const gulp          = require('gulp');
const minify        = require('gulp-minify');
const concat        = require('gulp-concat');
const plumber       = require('gulp-plumber');
const sass          = require('gulp-sass');
const sourceMaps    = require('gulp-sourcemaps');

/*====== DIRECTORIES ======*/
let dir_js      = './assets/js';
let dir_css     = './assets/css/pre/';
let dir_scss    = './assets/scss/style.scss';

// gulp.task('compress',() => {
//     return gulp.src([dir_js + '/dependencies/jquery-3.3.1.min.js'],[dir_js + '/dependencies/*.js'])
//         .pipe(plumber())
//         .pipe(minify())
//         .pipe(concat('dependencies.js'))
//         .pipe(gulp.dest(dir_js));
// });

gulp.task('watch',() => {
    return gulp.watch(dir_scss,['styles']);
});

gulp.task('styles',() => {
    gulp.src(dir_scss)
        .pipe(sourceMaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .on('error',sass.logError)
        .pipe(sourceMaps.write('./'))
        .pipe(gulp.dest(dir_css));
})