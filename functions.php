<?php

    //======================================================
    // DIRECTORIES
    //======================================================
    define('THEME_DIRECTORY',get_stylesheet_directory());
    define('THEME_URI',get_stylesheet_directory_uri());
    define('THEME_INCLUDE',THEME_URI . '/inc');
    define('THEME_JS', THEME_URI . '/assets/js');
    define('THEME_CSS', THEME_URI . '/assets/css');
    define('THEME_IMAGES',THEME_URI . '/assets/images');

    //======================================================
    // CUSTOM POST TYPES
    //======================================================
    require_once(TEMPLATEPATH . '/functions/cpt/custom-post-types.php');

    //======================================================
    // CORE TOOLBAR SETTINGS
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/core-toolbar-assets.php');

    //======================================================
    // ENQUEUE SCRIPTS
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/scripts.php');

    //======================================================
    // TEAMWORK API
    //======================================================
    require_once(TEMPLATEPATH . '/inc/teamwork/tw-clients-api.php');

    //======================================================
    // THEME SUPPORT
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/theme-support.php');

    //======================================================
    // DISABLE DEFAULT WIDGETS IN DASHBOARD
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/disable-dashboard-widgets.php');

    //======================================================
    // ADD WIDGETS IN DASHBOARD
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/add-dashboard-widgets.php');

    //======================================================
    // TASK REQUEST FORM MAILER
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/task-request-mailer.php');

    //======================================================
    // PM TO SUBMITTER FORM MAILER
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/pm-to-submitter-mailer.php');

    //======================================================
    // CLOSE TASK
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/close-task.php');

    //======================================================
    // TASK STATUS TAXONOMY
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/task-status.php');

    //======================================================
    // QUERY FORM DATA FOR SINGLE.PHP FILE
    //======================================================
    require_once(TEMPLATEPATH . '/functions/wordpress/form-data.php');





