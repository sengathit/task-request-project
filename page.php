<?php

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <div class="container">
                <div class="row">
                    <?php if(have_posts()):while(have_posts()):the_post();
                        if(is_page('etc-form')){
                            get_template_part('template-parts/page/content','etc-form');
                        }elseif(is_page('ces-form')){
                            get_template_part('template-parts/page/content','ces-form');
                        }elseif(is_page('thank-you')){
                            get_template_part('template-parts/page/content','thank-you');
                        }else{
                            get_template_part('template-parts/page/content','page');
                        }
                        endwhile;endif; ?>
                </div>
            </div>
		</main>
	</div>
</div>

<?php get_footer();
