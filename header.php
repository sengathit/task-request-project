<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
    <div id="page" class="site">
        <div class="site-content-contain">
            <div id="content" class="site-content">
            <?php if(is_user_logged_in()){ ?>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">Task Requests</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?php echo home_url() . '/ces' ; ?>">CES Tasks<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo home_url() . '/etc' ; ?>">ETC Tasks</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo home_url() . '/both' ; ?>">BOTH Tasks</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                
            <?php } 
