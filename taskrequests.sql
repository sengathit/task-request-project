-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 07, 2019 at 06:39 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `etc_tr`
--

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_commentmeta`
--

CREATE TABLE `etc_tr_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_comments`
--

CREATE TABLE `etc_tr_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_comments`
--

INSERT INTO `etc_tr_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-12-11 14:26:56', '2018-12-11 14:26:56', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_links`
--

CREATE TABLE `etc_tr_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_options`
--

CREATE TABLE `etc_tr_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_options`
--

INSERT INTO `etc_tr_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8888/tr', 'yes'),
(2, 'home', 'http://localhost:8888/tr', 'yes'),
(3, 'blogname', 'Task Request', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'slavanh@eightythreecreative.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:166:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:6:\"ces/?$\";s:23:\"index.php?post_type=ces\";s:36:\"ces/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?post_type=ces&feed=$matches[1]\";s:31:\"ces/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?post_type=ces&feed=$matches[1]\";s:23:\"ces/page/([0-9]{1,})/?$\";s:41:\"index.php?post_type=ces&paged=$matches[1]\";s:6:\"etc/?$\";s:23:\"index.php?post_type=etc\";s:36:\"etc/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?post_type=etc&feed=$matches[1]\";s:31:\"etc/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?post_type=etc&feed=$matches[1]\";s:23:\"etc/page/([0-9]{1,})/?$\";s:41:\"index.php?post_type=etc&paged=$matches[1]\";s:7:\"both/?$\";s:24:\"index.php?post_type=both\";s:37:\"both/feed/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=both&feed=$matches[1]\";s:32:\"both/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=both&feed=$matches[1]\";s:24:\"both/page/([0-9]{1,})/?$\";s:42:\"index.php?post_type=both&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:31:\"ces/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"ces/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"ces/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"ces/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"ces/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"ces/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:20:\"ces/([^/]+)/embed/?$\";s:36:\"index.php?ces=$matches[1]&embed=true\";s:24:\"ces/([^/]+)/trackback/?$\";s:30:\"index.php?ces=$matches[1]&tb=1\";s:44:\"ces/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?ces=$matches[1]&feed=$matches[2]\";s:39:\"ces/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?ces=$matches[1]&feed=$matches[2]\";s:32:\"ces/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?ces=$matches[1]&paged=$matches[2]\";s:39:\"ces/([^/]+)/comment-page-([0-9]{1,})/?$\";s:43:\"index.php?ces=$matches[1]&cpage=$matches[2]\";s:28:\"ces/([^/]+)(?:/([0-9]+))?/?$\";s:42:\"index.php?ces=$matches[1]&page=$matches[2]\";s:20:\"ces/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:30:\"ces/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:50:\"ces/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\"ces/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\"ces/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:26:\"ces/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:31:\"etc/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"etc/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"etc/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"etc/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"etc/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"etc/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:20:\"etc/([^/]+)/embed/?$\";s:36:\"index.php?etc=$matches[1]&embed=true\";s:24:\"etc/([^/]+)/trackback/?$\";s:30:\"index.php?etc=$matches[1]&tb=1\";s:44:\"etc/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?etc=$matches[1]&feed=$matches[2]\";s:39:\"etc/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?etc=$matches[1]&feed=$matches[2]\";s:32:\"etc/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?etc=$matches[1]&paged=$matches[2]\";s:39:\"etc/([^/]+)/comment-page-([0-9]{1,})/?$\";s:43:\"index.php?etc=$matches[1]&cpage=$matches[2]\";s:28:\"etc/([^/]+)(?:/([0-9]+))?/?$\";s:42:\"index.php?etc=$matches[1]&page=$matches[2]\";s:20:\"etc/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:30:\"etc/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:50:\"etc/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\"etc/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\"etc/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:26:\"etc/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:32:\"both/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"both/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"both/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"both/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"both/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"both/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"both/([^/]+)/embed/?$\";s:37:\"index.php?both=$matches[1]&embed=true\";s:25:\"both/([^/]+)/trackback/?$\";s:31:\"index.php?both=$matches[1]&tb=1\";s:45:\"both/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?both=$matches[1]&feed=$matches[2]\";s:40:\"both/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?both=$matches[1]&feed=$matches[2]\";s:33:\"both/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?both=$matches[1]&paged=$matches[2]\";s:40:\"both/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?both=$matches[1]&cpage=$matches[2]\";s:29:\"both/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?both=$matches[1]&page=$matches[2]\";s:21:\"both/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"both/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"both/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"both/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"both/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"both/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:47:\"status/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?status=$matches[1]&feed=$matches[2]\";s:42:\"status/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?status=$matches[1]&feed=$matches[2]\";s:23:\"status/([^/]+)/embed/?$\";s:39:\"index.php?status=$matches[1]&embed=true\";s:35:\"status/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?status=$matches[1]&paged=$matches[2]\";s:17:\"status/([^/]+)/?$\";s:28:\"index.php?status=$matches[1]\";s:49:\"priority/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?priority=$matches[1]&feed=$matches[2]\";s:44:\"priority/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?priority=$matches[1]&feed=$matches[2]\";s:25:\"priority/([^/]+)/embed/?$\";s:41:\"index.php?priority=$matches[1]&embed=true\";s:37:\"priority/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?priority=$matches[1]&paged=$matches[2]\";s:19:\"priority/([^/]+)/?$\";s:30:\"index.php?priority=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:40:\"index.php?&page_id=106&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'taskrequest', 'yes'),
(41, 'stylesheet', 'taskrequest', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '43764', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(84, 'page_on_front', '106', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'etc_tr_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:5:{i:1546889216;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1546914416;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1546957637;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1546958247;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(118, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1546869689;s:7:\"checked\";a:1:{s:11:\"taskrequest\";s:3:\"1.8\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(121, 'can_compress_scripts', '1', 'no'),
(135, 'theme_mods_twentynineteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1544538439;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(136, 'current_theme', 'MLP', 'yes'),
(137, 'theme_mods_taskrequest', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(138, 'theme_switched', '', 'yes'),
(141, 'recently_activated', 'a:0:{}', 'yes'),
(142, 'acf_version', '5.7.9', 'yes'),
(241, 'etc-task', 'larry@cable.com', 'yes'),
(546, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3NjoiYjNKa1pYSmZhV1E5TVRBNE5EQXpmSFI1Y0dVOVpHVjJaV3h2Y0dWeWZHUmhkR1U5TWpBeE55MHdOaTB4TkNBeE16b3lOam93T0E9PSI7czozOiJ1cmwiO3M6MjQ6Imh0dHA6Ly9sb2NhbGhvc3Q6ODg4OC90ciI7fQ==', 'yes'),
(578, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.2\";s:7:\"version\";s:5:\"5.0.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1546871866;s:15:\"version_checked\";s:5:\"5.0.2\";s:12:\"translations\";a:0:{}}', 'no'),
(604, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1546869689;s:7:\"checked\";a:3:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.7.9\";s:19:\"akismet/akismet.php\";s:3:\"4.1\";s:9:\"hello.php\";s:5:\"1.7.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:3:\"4.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/akismet.4.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(696, '_site_transient_timeout_browser_05a35df476881e356975dc07ce8a30ab', '1547046708', 'no'),
(697, '_site_transient_browser_05a35df476881e356975dc07ce8a30ab', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"71.0.3578.98\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(771, '_transient_is_multi_author', '1', 'yes'),
(774, '_transient_timeout_acf_plugin_updates', '1546956089', 'no'),
(775, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:86400;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.7.9\";}}', 'no'),
(784, '_site_transient_timeout_theme_roots', '1546873666', 'no'),
(785, '_site_transient_theme_roots', 'a:2:{s:11:\"taskrequest\";s:7:\"/themes\";s:17:\"taskrequestassets\";s:7:\"/themes\";}', 'no'),
(787, 'category_children', 'a:0:{}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_postmeta`
--

CREATE TABLE `etc_tr_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_postmeta`
--

INSERT INTO `etc_tr_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(1067, 272, '_wp_attached_file', '2018/12/texture-1.jpeg'),
(1068, 272, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:300;s:4:\"file\";s:22:\"2018/12/texture-1.jpeg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"texture-1-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"texture-1-200x300.jpeg\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1069, 271, '_thumbnail_id', '273'),
(1070, 273, '_wp_attached_file', '2018/12/ms_users-5.docx'),
(1071, 271, 'where', 'UA Local 100 - Web Maintenance'),
(1072, 271, 'start_date', '12/29/2018'),
(1073, 271, 'due_date', '12/30/2018'),
(1074, 271, 'resources', 'Nah'),
(1075, 271, 'additional_details', ''),
(1076, 271, 'needs', 'I need stuff'),
(1077, 271, 'tasks', 'I am required'),
(1078, 271, 'upload', 'a:5:{s:4:\"name\";a:2:{i:0;s:12:\"texture.jpeg\";i:1;s:13:\"ms_users.docx\";}s:4:\"type\";a:2:{i:0;s:10:\"image/jpeg\";i:1;s:71:\"application/vnd.openxmlformats-officedocument.wordprocessingml.document\";}s:8:\"tmp_name\";a:2:{i:0;s:36:\"/Applications/MAMP/tmp/php/phpHmHE5r\";i:1;s:36:\"/Applications/MAMP/tmp/php/phpNQIr5k\";}s:5:\"error\";a:2:{i:0;i:0;i:1;i:0;}s:4:\"size\";a:2:{i:0;i:14083;i:1;i:166703;}}'),
(1079, 271, 'submitter_email', 'sengathit.l@gmail.com'),
(1080, 271, '_edit_lock', '1546021304:1'),
(1081, 274, '_edit_last', '1'),
(1082, 274, '_edit_lock', '1546013457:1'),
(1083, 276, '_edit_last', '1'),
(1084, 276, '_edit_lock', '1546013304:1'),
(1085, 278, '_edit_last', '1'),
(1086, 278, '_edit_lock', '1546013315:1'),
(1087, 280, '_edit_last', '1'),
(1088, 280, '_edit_lock', '1546013392:1'),
(1089, 282, '_edit_last', '1'),
(1090, 282, '_edit_lock', '1546013469:1'),
(1091, 283, '_edit_last', '1'),
(1092, 283, '_edit_lock', '1546013557:1'),
(1093, 285, '_edit_last', '1'),
(1094, 285, '_edit_lock', '1546541643:1'),
(1095, 287, '_edit_last', '1'),
(1096, 287, '_edit_lock', '1546013716:1'),
(1097, 290, '_wp_attached_file', '2018/12/dummy_upload-6.docx'),
(1098, 289, '_thumbnail_id', '290'),
(1099, 289, 'where', 'The Grand - Photography | Videography'),
(1100, 289, 'start_date', '12/28/2018'),
(1101, 289, 'due_date', '12/28/2018'),
(1102, 289, 'resources', 'hah'),
(1103, 289, 'additional_details', 'nah'),
(1104, 289, 'needs', 'needs'),
(1105, 289, 'tasks', 'tasks'),
(1106, 289, 'upload', ''),
(1107, 289, 'submitter_email', 'sengathit.l@gmail.com'),
(1108, 289, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1109, 291, '_thumbnail_id', '292'),
(1110, 291, 'where', 'AT&T - Videos'),
(1111, 291, 'start_date', '12/28/2018'),
(1112, 291, 'due_date', '12/28/2018'),
(1113, 291, 'resources', 'asdg'),
(1114, 291, 'additional_details', 'asdgag'),
(1115, 291, 'needs', 'asdg'),
(1116, 291, 'tasks', 'asdggs'),
(1117, 291, 'upload', 'a:5:{s:4:\"name\";a:1:{i:0;s:0:\"\";}s:4:\"type\";a:1:{i:0;s:0:\"\";}s:8:\"tmp_name\";a:1:{i:0;s:0:\"\";}s:5:\"error\";a:1:{i:0;i:4;}s:4:\"size\";a:1:{i:0;i:0;}}'),
(1118, 291, 'submitter_email', 'sengathit.l@gmail.com'),
(1119, 291, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1120, 289, '_edit_lock', '1546621028:1'),
(1121, 293, '_thumbnail_id', '294'),
(1122, 293, 'where', 'The Tot - Collateral'),
(1123, 293, 'start_date', '12/28/2018'),
(1124, 293, 'due_date', '12/28/2018'),
(1125, 293, 'resources', 'asdgdags'),
(1126, 293, 'additional_details', ''),
(1127, 293, 'needs', 'asdg'),
(1128, 293, 'tasks', 'adsg'),
(1129, 293, 'upload', 'a:5:{s:4:\"name\";a:1:{i:0;s:0:\"\";}s:4:\"type\";a:1:{i:0;s:0:\"\";}s:8:\"tmp_name\";a:1:{i:0;s:0:\"\";}s:5:\"error\";a:1:{i:0;i:4;}s:4:\"size\";a:1:{i:0;i:0;}}'),
(1130, 293, 'submitter_email', 'sengathit.l@gmail.com'),
(1131, 293, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1132, 295, 'where', 'AT&T - Videos'),
(1133, 295, 'start_date', '12/28/2018'),
(1134, 295, 'due_date', '12/28/2018'),
(1135, 295, 'resources', 'asdgdga'),
(1136, 295, 'additional_details', 'asdgsag'),
(1137, 295, 'needs', 'asdgasg'),
(1138, 295, 'tasks', 'asdgdsag'),
(1139, 295, 'upload', NULL),
(1140, 295, 'submitter_email', 'sengathit.l@gmail.com'),
(1141, 295, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1142, 296, 'where', 'CES - Branch Requests: Business Cards'),
(1143, 296, 'start_date', '12/28/2018'),
(1144, 296, 'due_date', '12/28/2018'),
(1145, 296, 'resources', 'asdgga'),
(1146, 296, 'additional_details', 'asdgasg'),
(1147, 296, 'needs', 'asdgadsg'),
(1148, 296, 'tasks', 'asdgg'),
(1149, 296, 'upload', NULL),
(1150, 296, 'submitter_email', 'sengathit.l@gmail.com'),
(1151, 296, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1152, 297, 'where', '400 Record - App'),
(1153, 297, 'start_date', '12/28/2018'),
(1154, 297, 'due_date', '12/28/2018'),
(1155, 297, 'resources', 'asdg'),
(1156, 297, 'additional_details', 'asdgdag'),
(1157, 297, 'needs', 'asdg'),
(1158, 297, 'tasks', 'asdg'),
(1159, 297, 'upload', ''),
(1160, 297, 'submitter_email', 'sengathit.l@gmail.com'),
(1161, 297, 'project_manager_email', 'sengathit.lavanh@gmail.com'),
(1162, 297, '_edit_lock', '1546026929:1'),
(1163, 297, '_edit_last', '1'),
(1164, 297, '_needs', 'field_5c2649f0733b4'),
(1165, 297, '_tasks', 'field_5c264aba5fcd4'),
(1166, 297, '_where', 'field_5c2648e78be7f'),
(1167, 297, '_start_date', 'field_5c264b354aab4'),
(1168, 297, '_additional_details', 'field_5c264bdc6d194'),
(1169, 297, '_upload', 'field_5c264c25dcc64'),
(1170, 297, '_resources', 'field_5c264c59214f0'),
(1171, 298, '_edit_lock', '1546613237:1'),
(1172, 298, '_edit_last', '1'),
(1173, 106, '_edit_lock', '1546526586:1'),
(1174, 300, 'where', 'CES - CES Online Photography (2018)'),
(1175, 300, 'start_date', '01/03/2019'),
(1176, 300, 'due_date', '01/03/2019'),
(1177, 300, 'resources', 'adsgasg'),
(1178, 300, 'additional_details', 'nope'),
(1179, 300, 'needs', 'images'),
(1180, 300, 'tasks', 'everything'),
(1181, 300, 'upload', ''),
(1182, 300, 'submitter_email', 'sengathit.l@gmail.com'),
(1183, 300, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1184, 300, '_edit_lock', '1546623645:1'),
(1185, 301, 'where', '400 Record - Social Retainer 2019'),
(1186, 301, 'start_date', '01/03/2019'),
(1187, 301, 'due_date', '01/03/2019'),
(1188, 301, 'resources', 'asdgasg'),
(1189, 301, 'additional_details', 'adsgags'),
(1190, 301, 'needs', 'adsgasdg'),
(1191, 301, 'tasks', 'asdgadsg'),
(1192, 301, 'upload', ''),
(1193, 301, 'submitter_email', 'sengathit.l@gmail.com'),
(1194, 301, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1195, 302, 'where', 'Bullion - 2018 Holiday Photography'),
(1196, 302, 'start_date', '01/18/2019'),
(1197, 302, 'due_date', '01/25/2019'),
(1198, 302, 'resources', 'asdgag'),
(1199, 302, 'additional_details', 'asdgag'),
(1200, 302, 'needs', 'adgadg'),
(1201, 302, 'tasks', 'adfhdh'),
(1202, 302, 'upload', NULL),
(1203, 302, 'submitter_email', 'sengathit.l@gmail.com'),
(1204, 302, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1205, 304, '_wp_attached_file', '2019/01/ms_users.docx'),
(1206, 303, '_thumbnail_id', '304'),
(1207, 303, 'where', 'AT&T - Videos'),
(1208, 303, 'start_date', '01/03/2019'),
(1209, 303, 'due_date', '01/03/2019'),
(1210, 303, 'resources', 'adgadfgh'),
(1211, 303, 'additional_details', 'asdgasg'),
(1212, 303, 'needs', 'adgdag'),
(1213, 303, 'tasks', 'adsgdag'),
(1214, 303, 'upload', 'a:5:{s:4:\"name\";a:1:{i:0;s:13:\"ms_users.docx\";}s:4:\"type\";a:1:{i:0;s:71:\"application/vnd.openxmlformats-officedocument.wordprocessingml.document\";}s:8:\"tmp_name\";a:1:{i:0;s:36:\"/Applications/MAMP/tmp/php/phpufJWXe\";}s:5:\"error\";a:1:{i:0;i:0;}s:4:\"size\";a:1:{i:0;i:166703;}}'),
(1215, 303, 'submitter_email', 'sengathit.l@gmail.com'),
(1216, 303, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1217, 306, '_wp_attached_file', '2019/01/dummy_upload.docx'),
(1218, 305, '_thumbnail_id', '306'),
(1219, 305, 'where', 'CES - CES Online Photography (2018)'),
(1220, 305, 'start_date', '01/24/2019'),
(1221, 305, 'due_date', '01/28/2019'),
(1222, 305, 'resources', 'Nope'),
(1223, 305, 'additional_details', 'No details'),
(1224, 305, 'needs', 'I needs it'),
(1225, 305, 'tasks', 'I requires it'),
(1226, 305, 'upload', 'a:5:{s:4:\"name\";a:1:{i:0;s:17:\"dummy_upload.docx\";}s:4:\"type\";a:1:{i:0;s:71:\"application/vnd.openxmlformats-officedocument.wordprocessingml.document\";}s:8:\"tmp_name\";a:1:{i:0;s:36:\"/Applications/MAMP/tmp/php/phpK1mRD4\";}s:5:\"error\";a:1:{i:0;i:0;}s:4:\"size\";a:1:{i:0;i:26478;}}'),
(1227, 305, 'submitter_email', 'sengathit.l@gmail.com'),
(1228, 305, 'project_manager_email', 'slavanh@eightythreecreative.com'),
(1229, 307, '_edit_lock', '1546613558:1'),
(1230, 301, '_edit_lock', '1546616504:1'),
(1231, 301, '_edit_last', '1'),
(1232, 301, '_needs', 'field_5c2649f0733b4'),
(1233, 301, '_tasks', 'field_5c264aba5fcd4'),
(1234, 301, '_where', 'field_5c2648e78be7f'),
(1235, 301, '_start_date', 'field_5c264b354aab4'),
(1236, 301, '_additional_details', 'field_5c264bdc6d194'),
(1237, 301, '_upload', 'field_5c264c25dcc64'),
(1238, 301, '_resources', 'field_5c264c59214f0'),
(1239, 289, '_edit_last', '1'),
(1240, 289, '_needs', 'field_5c2649f0733b4'),
(1241, 289, '_tasks', 'field_5c264aba5fcd4'),
(1242, 289, '_where', 'field_5c2648e78be7f'),
(1243, 289, '_start_date', 'field_5c264b354aab4'),
(1244, 289, '_additional_details', 'field_5c264bdc6d194'),
(1245, 289, '_upload', 'field_5c264c25dcc64'),
(1246, 289, '_resources', 'field_5c264c59214f0'),
(1247, 300, '_edit_last', '1'),
(1248, 300, '_needs', 'field_5c2649f0733b4'),
(1249, 300, '_tasks', 'field_5c264aba5fcd4'),
(1250, 300, '_where', 'field_5c2648e78be7f'),
(1251, 300, '_start_date', 'field_5c264b354aab4'),
(1252, 300, '_additional_details', 'field_5c264bdc6d194'),
(1253, 300, '_upload', 'field_5c264c25dcc64'),
(1254, 300, '_resources', 'field_5c264c59214f0'),
(1255, 298, '_wp_trash_meta_status', 'publish'),
(1256, 298, '_wp_trash_meta_time', '1546640528'),
(1257, 298, '_wp_desired_post_slug', 'ajax'),
(1258, 109, '_edit_lock', '1546640594:1');

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_posts`
--

CREATE TABLE `etc_tr_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_posts`
--

INSERT INTO `etc_tr_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(106, 1, '2018-12-14 14:45:05', '2018-12-14 14:45:05', '', 'Task Request', '', 'publish', 'closed', 'closed', '', 'etc-form', '', '', '2018-12-19 20:52:05', '2018-12-19 20:52:05', '', 0, 'http://localhost:8888/tr/?page_id=106', 0, 'page', '', 0),
(109, 1, '2018-12-14 15:08:28', '2018-12-14 15:08:28', '', 'Thank You', '', 'publish', 'closed', 'closed', '', 'thank-you', '', '', '2018-12-14 15:08:28', '2018-12-14 15:08:28', '', 0, 'http://localhost:8888/tr/?page_id=109', 0, 'page', '', 0),
(150, 1, '2018-12-14 18:52:43', '2018-12-14 18:52:43', '', 'Fake Form', '', 'publish', 'open', 'open', '', 'fake-form', '', '', '2018-12-14 18:52:43', '2018-12-14 18:52:43', '', 0, 'http://localhost:8888/tr/?p=150', 0, 'post', '', 0),
(151, 1, '2018-12-14 18:52:43', '2018-12-14 18:52:43', '', 'Fake Form', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2018-12-14 18:52:43', '2018-12-14 18:52:43', '', 150, 'http://localhost:8888/tr/150-revision-v1/', 0, 'revision', '', 0),
(163, 1, '2018-12-17 19:26:15', '2018-12-17 19:26:15', '', 'Front Page', '', 'publish', 'closed', 'closed', '', 'front-page', '', '', '2018-12-17 19:26:15', '2018-12-17 19:26:15', '', 0, 'http://localhost:8888/tr/?page_id=163', 0, 'page', '', 0),
(164, 1, '2018-12-17 19:26:15', '2018-12-17 19:26:15', '', 'Front Page', '', 'inherit', 'closed', 'closed', '', '163-revision-v1', '', '', '2018-12-17 19:26:15', '2018-12-17 19:26:15', '', 163, 'http://localhost:8888/tr/163-revision-v1/', 0, 'revision', '', 0),
(173, 1, '2018-12-18 16:48:07', '2018-12-18 16:48:07', '', 'Train broke', '', 'inherit', 'closed', 'closed', '', '172-autosave-v1', '', '', '2018-12-18 16:48:07', '2018-12-18 16:48:07', '', 172, 'http://localhost:8888/tr/172-autosave-v1/', 0, 'revision', '', 0),
(204, 0, '2018-12-19 16:56:38', '2018-12-19 16:56:38', 'hey from body', 'Testing out dynamic form', '', 'publish', 'closed', 'closed', '', 'testing-out-dynamic-form', '', '', '2018-12-19 16:56:38', '2018-12-19 16:56:38', '', 0, 'http://localhost:8888/tr/testing-out-dynamic-form/', 0, '1', '', 0),
(214, 0, '2018-12-19 20:07:30', '2018-12-19 20:07:30', 'hey from body', 'War room', '', 'publish', 'open', 'open', '', 'war-room', '', '', '2018-12-19 20:07:30', '2018-12-19 20:07:30', '', 0, 'http://localhost:8888/tr/war-room/', 0, 'post', '', 0),
(215, 1, '2018-12-19 20:52:04', '2018-12-19 20:52:04', '', 'Task Request', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2018-12-19 20:52:04', '2018-12-19 20:52:04', '', 106, 'http://localhost:8888/tr/106-revision-v1/', 0, 'revision', '', 0),
(216, 0, '2018-12-19 20:56:07', '2018-12-19 20:56:07', 'hey from body', 'Testing 1', '', 'publish', 'open', 'open', '', 'testing-1', '', '', '2018-12-19 20:56:07', '2018-12-19 20:56:07', '', 0, 'http://localhost:8888/tr/testing-1/', 0, 'post', '', 0),
(271, 0, '2018-12-28 15:35:19', '2018-12-28 15:35:19', 'hey from body', 'testing 1', '', 'publish', 'closed', 'closed', '', 'testing-1', '', '', '2018-12-28 15:35:19', '2018-12-28 15:35:19', '', 0, 'http://localhost:8888/tr/etc/testing-1/', 0, 'etc', '', 0),
(272, 1, '2018-12-28 15:35:19', '2018-12-28 15:35:19', '', 'texture-1', '', 'inherit', 'open', 'closed', '', 'texture-1', '', '', '2018-12-28 15:35:19', '2018-12-28 15:35:19', '', 271, 'http://localhost:8888/tr/wp-content/uploads2018/12/texture-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(273, 1, '2018-12-28 15:35:19', '2018-12-28 15:35:19', '', 'ms_users-5', '', 'inherit', 'open', 'closed', '', 'ms_users-5', '', '', '2018-12-28 15:35:19', '2018-12-28 15:35:19', '', 271, 'http://localhost:8888/tr/wp-content/uploads2018/12/ms_users-5.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(274, 1, '2018-12-28 16:04:58', '2018-12-28 16:04:58', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Where', 'where', 'publish', 'closed', 'closed', '', 'group_5c26489880f0b', '', '', '2018-12-28 16:11:07', '2018-12-28 16:11:07', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=274', 4, 'acf-field-group', '', 0),
(275, 1, '2018-12-28 16:04:58', '2018-12-28 16:04:58', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:130:\"Where should this live in TW? *\r\nPlease link the specific TeamWork Project name/task list (i.e. \"ETC - New Hires\"), if applicable.\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Where', 'where', 'publish', 'closed', 'closed', '', 'field_5c2648e78be7f', '', '', '2018-12-28 16:04:58', '2018-12-28 16:04:58', '', 274, 'http://localhost:8888/tr/?post_type=acf-field&p=275', 0, 'acf-field', '', 0),
(276, 1, '2018-12-28 16:06:03', '2018-12-28 16:06:03', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Needs', 'needs', 'publish', 'closed', 'closed', '', 'group_5c2649b6463e1', '', '', '2018-12-28 16:10:45', '2018-12-28 16:10:45', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=276', 2, 'acf-field-group', '', 0),
(277, 1, '2018-12-28 16:06:33', '2018-12-28 16:06:33', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:105:\"What do you need? *\r\nUse this section to include any and all details pertaining to what needs to be done.\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Needs', 'needs', 'publish', 'closed', 'closed', '', 'field_5c2649f0733b4', '', '', '2018-12-28 16:06:33', '2018-12-28 16:06:33', '', 276, 'http://localhost:8888/tr/?post_type=acf-field&p=277', 0, 'acf-field', '', 0),
(278, 1, '2018-12-28 16:09:52', '2018-12-28 16:09:52', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Tasks', 'tasks', 'publish', 'closed', 'closed', '', 'group_5c264a20d91f8', '', '', '2018-12-28 16:10:56', '2018-12-28 16:10:56', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=278', 3, 'acf-field-group', '', 0),
(279, 1, '2018-12-28 16:09:52', '2018-12-28 16:09:52', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:401:\"What tasks are required to complete this? And who is doing them? *\r\nPlease include any details regarding other tasks needed to be required. For example, if we\'re creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you\'ve been given estimates or resources (i.e. specific people) for these tasks, please also include that information.\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tasks', 'tasks', 'publish', 'closed', 'closed', '', 'field_5c264aba5fcd4', '', '', '2018-12-28 16:09:52', '2018-12-28 16:09:52', '', 278, 'http://localhost:8888/tr/?post_type=acf-field&p=279', 0, 'acf-field', '', 0),
(280, 1, '2018-12-28 16:11:59', '2018-12-28 16:11:59', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Start Date', 'start-date', 'publish', 'closed', 'closed', '', 'group_5c264b22d8155', '', '', '2018-12-28 16:12:12', '2018-12-28 16:12:12', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=280', 6, 'acf-field-group', '', 0),
(281, 1, '2018-12-28 16:11:59', '2018-12-28 16:11:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Start Date', 'start_date', 'publish', 'closed', 'closed', '', 'field_5c264b354aab4', '', '', '2018-12-28 16:11:59', '2018-12-28 16:11:59', '', 280, 'http://localhost:8888/tr/?post_type=acf-field&p=281', 0, 'acf-field', '', 0),
(282, 1, '2018-12-28 16:13:03', '2018-12-28 16:13:03', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Due Date', 'due-date', 'publish', 'closed', 'closed', '', 'group_5c264b72ea573', '', '', '2018-12-28 16:13:03', '2018-12-28 16:13:03', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=282', 7, 'acf-field-group', '', 0),
(283, 1, '2018-12-28 16:14:16', '2018-12-28 16:14:16', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Additional Details', 'additional-details', 'publish', 'closed', 'closed', '', 'group_5c264bb0ac0c5', '', '', '2018-12-28 16:14:50', '2018-12-28 16:14:50', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=283', 8, 'acf-field-group', '', 0),
(284, 1, '2018-12-28 16:14:50', '2018-12-28 16:14:50', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:95:\"Any additional details?\r\nIs there any extra context around this task you would like us to know?\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Additional_Details', 'additional_details', 'publish', 'closed', 'closed', '', 'field_5c264bdc6d194', '', '', '2018-12-28 16:14:50', '2018-12-28 16:14:50', '', 283, 'http://localhost:8888/tr/?post_type=acf-field&p=284', 0, 'acf-field', '', 0),
(285, 1, '2018-12-28 16:15:49', '2018-12-28 16:15:49', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Upload', 'upload', 'publish', 'closed', 'closed', '', 'group_5c264c0fed193', '', '', '2019-01-03 18:56:25', '2019-01-03 18:56:25', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=285', 8, 'acf-field-group', '', 0),
(286, 1, '2018-12-28 16:15:49', '2018-12-28 16:15:49', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:23:\"File upload (if needed)\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Upload', 'upload', 'publish', 'closed', 'closed', '', 'field_5c264c25dcc64', '', '', '2019-01-03 18:56:25', '2019-01-03 18:56:25', '', 285, 'http://localhost:8888/tr/?post_type=acf-field&#038;p=286', 0, 'acf-field', '', 0),
(287, 1, '2018-12-28 16:16:38', '2018-12-28 16:16:38', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"ces\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"etc\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"both\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Resources', 'resources', 'publish', 'closed', 'closed', '', 'group_5c264c4589b99', '', '', '2018-12-28 16:17:40', '2018-12-28 16:17:40', '', 0, 'http://localhost:8888/tr/?post_type=acf-field-group&#038;p=287', 9, 'acf-field-group', '', 0),
(288, 1, '2018-12-28 16:16:38', '2018-12-28 16:16:38', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:103:\"Will this task require any resources or involvement from the CES Team? *\r\nIf so, please provide details\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Resources', 'resources', 'publish', 'closed', 'closed', '', 'field_5c264c59214f0', '', '', '2018-12-28 16:16:38', '2018-12-28 16:16:38', '', 287, 'http://localhost:8888/tr/?post_type=acf-field&p=288', 0, 'acf-field', '', 0),
(289, 1, '2018-12-28 16:21:04', '2018-12-28 16:21:04', 'hey from body', 'Testing PM email', '', 'publish', 'closed', 'closed', '', 'testing-pm-email', '', '', '2019-01-04 16:59:32', '2019-01-04 16:59:32', '', 0, 'http://localhost:8888/tr/ces/testing-pm-email/', 0, 'ces', '', 0),
(290, 1, '2018-12-28 16:21:04', '2018-12-28 16:21:04', '', 'dummy_upload-6', '', 'inherit', 'open', 'closed', '', 'dummy_upload-6', '', '', '2018-12-28 16:21:04', '2018-12-28 16:21:04', '', 289, 'http://localhost:8888/tr/wp-content/uploads2018/12/dummy_upload-6.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(291, 0, '2018-12-28 17:22:36', '2018-12-28 17:22:36', 'hey from body', 'testing close task form', '', 'publish', 'closed', 'closed', '', 'testing-close-task-form', '', '', '2018-12-28 17:22:36', '2018-12-28 17:22:36', '', 0, 'http://localhost:8888/tr/both/testing-close-task-form/', 0, 'both', '', 0),
(292, 1, '2018-12-28 17:22:36', '2018-12-28 17:22:36', '', '', '', 'inherit', 'open', 'closed', '', '292', '', '', '2018-12-28 17:22:36', '2018-12-28 17:22:36', '', 291, 'http://localhost:8888/tr/wp-content/uploads', 0, 'attachment', '', 0),
(293, 0, '2018-12-28 18:31:13', '2018-12-28 18:31:13', 'hey from body', 'emails not sending to multiple', '', 'publish', 'closed', 'closed', '', 'emails-not-sending-to-multiple', '', '', '2018-12-28 18:31:13', '2018-12-28 18:31:13', '', 0, 'http://localhost:8888/tr/etc/emails-not-sending-to-multiple/', 0, 'etc', '', 0),
(294, 1, '2018-12-28 18:31:13', '2018-12-28 18:31:13', '', '', '', 'inherit', 'open', 'closed', '', '294', '', '', '2018-12-28 18:31:13', '2018-12-28 18:31:13', '', 293, 'http://localhost:8888/tr/wp-content/uploads', 0, 'attachment', '', 0),
(295, 0, '2018-12-28 18:43:15', '2018-12-28 18:43:15', 'hey from body', 'as', '', 'publish', 'closed', 'closed', '', 'as', '', '', '2018-12-28 18:43:15', '2018-12-28 18:43:15', '', 0, 'http://localhost:8888/tr/etc/as/', 0, 'etc', '', 0),
(296, 0, '2018-12-28 19:22:50', '2018-12-28 19:22:50', 'hey from body', 'asdg', '', 'publish', 'closed', 'closed', '', 'asdg', '', '', '2018-12-28 19:22:50', '2018-12-28 19:22:50', '', 0, 'http://localhost:8888/tr/etc/asdg/', 0, 'etc', '', 0),
(297, 1, '2018-12-28 19:27:37', '2018-12-28 19:27:37', 'hey from body', 'Random ness', '', 'publish', 'closed', 'closed', '', '297', '', '', '2018-12-28 19:57:52', '2018-12-28 19:57:52', '', 0, 'http://localhost:8888/tr/etc/297/', 0, 'etc', '', 0),
(298, 1, '2019-01-03 14:45:24', '2019-01-03 14:45:24', '', 'Ajax', '', 'trash', 'closed', 'closed', '', 'ajax__trashed', '', '', '2019-01-04 22:22:08', '2019-01-04 22:22:08', '', 0, 'http://localhost:8888/tr/?page_id=298', 0, 'page', '', 0),
(299, 1, '2019-01-03 14:45:24', '2019-01-03 14:45:24', '', 'Ajax', '', 'inherit', 'closed', 'closed', '', '298-revision-v1', '', '', '2019-01-03 14:45:24', '2019-01-03 14:45:24', '', 298, 'http://localhost:8888/tr/298-revision-v1/', 0, 'revision', '', 0),
(300, 1, '2019-01-03 17:55:06', '2019-01-03 17:55:06', 'hey from body', 'I need images', '', 'publish', 'closed', 'closed', '', 'i-need-images', '', '', '2019-01-04 17:43:08', '2019-01-04 17:43:08', '', 0, 'http://localhost:8888/tr/etc/i-need-images/', 0, 'etc', '', 0),
(301, 1, '2019-01-03 18:53:19', '2019-01-03 18:53:19', 'hey from body', 'asdgdags', '', 'publish', 'closed', 'closed', '', 'asdgdags', '', '', '2019-01-04 15:41:44', '2019-01-04 15:41:44', '', 0, 'http://localhost:8888/tr/ces/asdgdags/', 0, 'ces', '', 0),
(302, 0, '2019-01-03 18:56:56', '2019-01-03 18:56:56', 'hey from body', 'asdgdag', '', 'publish', 'closed', 'closed', '', 'asdgdag', '', '', '2019-01-03 18:56:56', '2019-01-03 18:56:56', '', 0, 'http://localhost:8888/tr/both/asdgdag/', 0, 'both', '', 0),
(303, 0, '2019-01-03 20:16:46', '2019-01-03 20:16:46', 'hey from body', 'Testing 1', '', 'publish', 'closed', 'closed', '', 'testing-1', '', '', '2019-01-03 20:16:46', '2019-01-03 20:16:46', '', 0, 'http://localhost:8888/tr/both/testing-1/', 0, 'both', '', 0),
(304, 1, '2019-01-03 20:16:46', '2019-01-03 20:16:46', '', 'ms_users', '', 'inherit', 'open', 'closed', '', 'ms_users', '', '', '2019-01-03 20:16:46', '2019-01-03 20:16:46', '', 303, 'http://localhost:8888/tr/wp-content/uploads2019/01/ms_users.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(305, 0, '2019-01-03 20:24:00', '2019-01-03 20:24:00', 'hey from body', 'update email layout', '', 'publish', 'closed', 'closed', '', 'update-email-layout', '', '', '2019-01-03 20:24:00', '2019-01-03 20:24:00', '', 0, 'http://localhost:8888/tr/both/update-email-layout/', 0, 'both', '', 0),
(306, 1, '2019-01-03 20:24:00', '2019-01-03 20:24:00', '', 'dummy_upload', '', 'inherit', 'open', 'closed', '', 'dummy_upload', '', '', '2019-01-03 20:24:00', '2019-01-03 20:24:00', '', 305, 'http://localhost:8888/tr/wp-content/uploads2019/01/dummy_upload.docx', 0, 'attachment', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 0),
(307, 1, '2019-01-04 14:55:00', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-04 14:55:00', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/tr/?page_id=307', 0, 'page', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_termmeta`
--

CREATE TABLE `etc_tr_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_terms`
--

CREATE TABLE `etc_tr_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_terms`
--

INSERT INTO `etc_tr_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'CES', 'ces', 0),
(3, 'Task', 'task', 0),
(5, 'Project', 'project', 0),
(6, 'Priority', 'priority', 0),
(7, 'ETC', 'etc', 0),
(8, 'Both', 'both', 0),
(9, 'Open', 'open', 0),
(10, 'Archive', 'archive', 0),
(11, 'Low', 'low', 0),
(12, 'Medium', 'medium', 0),
(13, 'High', 'high', 0),
(14, 'ETC', 'etc', 0);

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_term_relationships`
--

CREATE TABLE `etc_tr_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_term_relationships`
--

INSERT INTO `etc_tr_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(271, 5, 0),
(271, 7, 0),
(271, 10, 0),
(271, 13, 0),
(289, 1, 0),
(289, 5, 0),
(289, 10, 0),
(289, 13, 0),
(291, 3, 0),
(291, 8, 0),
(291, 9, 0),
(291, 12, 0),
(293, 3, 0),
(293, 7, 0),
(293, 10, 0),
(293, 13, 0),
(295, 5, 0),
(295, 7, 0),
(295, 10, 0),
(295, 12, 0),
(296, 3, 0),
(296, 7, 0),
(296, 10, 0),
(296, 12, 0),
(297, 3, 0),
(297, 7, 0),
(297, 10, 0),
(297, 11, 0),
(300, 5, 0),
(300, 7, 0),
(300, 9, 0),
(300, 11, 0),
(301, 1, 0),
(301, 5, 0),
(301, 10, 0),
(301, 13, 0),
(302, 5, 0),
(302, 8, 0),
(302, 9, 0),
(302, 13, 0),
(303, 3, 0),
(303, 8, 0),
(303, 9, 0),
(303, 11, 0),
(305, 3, 0),
(305, 8, 0),
(305, 9, 0),
(305, 13, 0);

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_term_taxonomy`
--

CREATE TABLE `etc_tr_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_term_taxonomy`
--

INSERT INTO `etc_tr_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', 'ces', 0, 2),
(3, 3, 'post_tag', 'task', 0, 6),
(5, 5, 'post_tag', 'project', 0, 6),
(6, 6, 'post_tag', 'priority', 0, 0),
(7, 7, 'category', 'etc', 0, 6),
(8, 8, 'category', 'both', 0, 4),
(9, 9, 'status', 'open', 0, 5),
(10, 10, 'status', 'closed', 0, 7),
(11, 11, 'priority', 'low', 0, 3),
(12, 12, 'priority', 'medium', 0, 3),
(13, 13, 'priority', 'high', 0, 6),
(14, 14, 'post_tag', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_usermeta`
--

CREATE TABLE `etc_tr_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_usermeta`
--

INSERT INTO `etc_tr_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'sengathit'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'etc_tr_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'etc_tr_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'etc_tr_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(19, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(20, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:13:\"ces_dashboard\";s:4:\"side\";s:35:\"etc_dashboard,etc_archive_dashboard\";s:7:\"column3\";s:0:\"\";s:7:\"column4\";s:0:\"\";}'),
(22, 1, 'closedpostboxes_ces', 'a:0:{}'),
(23, 1, 'metaboxhidden_ces', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(24, 1, 'closedpostboxes_etc', 'a:0:{}'),
(25, 1, 'metaboxhidden_etc', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(26, 1, 'meta-box-order_etc', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:70:\"submitdiv,categorydiv,tagsdiv-post_tag,tagsdiv-status,tagsdiv-priority\";s:6:\"normal\";s:271:\"acf-group_5c13cd01cc35b,acf-group_5c13cc73bef2a,acf-group_5c13cccc5ce82,acf-group_5c13cd819a6bd,acf-group_5c13cdd30e911,acf-group_5c13c82eec6fc,acf-group_5c13ca8dceb9a,acf-group_5c13c7ef382f9,acf-group_5c13c8a3e3f1f,acf-group_5c13c979348a3,acf-group_5c13cb03ad548,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(27, 1, 'screen_layout_etc', '2'),
(28, 1, 'closedpostboxes_post', 'a:0:{}'),
(29, 1, 'metaboxhidden_post', 'a:8:{i:0;s:23:\"acf-group_5c13cd01cc35b\";i:1;s:23:\"acf-group_5c13cccc5ce82\";i:2;s:23:\"acf-group_5c13cd819a6bd\";i:3;s:23:\"acf-group_5c13c8a3e3f1f\";i:4;s:23:\"acf-group_5c13cdd30e911\";i:5;s:23:\"acf-group_5c13cc73bef2a\";i:6;s:23:\"acf-group_5c13c7ef382f9\";i:7;s:23:\"acf-group_5c13cb03ad548\";}'),
(30, 1, 'session_tokens', 'a:5:{s:64:\"f2948c7370c0268d6404b035b6efe250bc1756de0754982858f8a677bcf11d74\";a:4:{s:10:\"expiration\";i:1547043081;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1545833481;}s:64:\"db76d718ef24390335e25ec2b0ffef376064e495312934e7e6f94e5914e72cc8\";a:4:{s:10:\"expiration\";i:1547048978;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1545839378;}s:64:\"414181bef112ce38f5bea5b4a25a7a0dc33021f050eeacae205d4c64f2a4958c\";a:4:{s:10:\"expiration\";i:1547051862;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1545842262;}s:64:\"715cda69732880295f3d02a855bdf468e6d303c573bc1fb7329db826546fc348\";a:4:{s:10:\"expiration\";i:1547056977;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1545847377;}s:64:\"068c95f8bc79b807598c969f0a4266d272c88952b1657dbf84913b420efde18e\";a:4:{s:10:\"expiration\";i:1547832620;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1546623020;}}'),
(31, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(32, 1, 'metaboxhidden_nav-menus', 'a:7:{i:0;s:17:\"add-post-type-ces\";i:1;s:17:\"add-post-type-etc\";i:2;s:18:\"add-post-type-both\";i:3;s:12:\"add-post_tag\";i:4;s:15:\"add-post_format\";i:5;s:10:\"add-status\";i:6;s:12:\"add-priority\";}');

-- --------------------------------------------------------

--
-- Table structure for table `etc_tr_users`
--

CREATE TABLE `etc_tr_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `etc_tr_users`
--

INSERT INTO `etc_tr_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'sengathit', '$P$BX4X/5xw7PXTLhlvyknBGQ2mN75haq1', 'sengathit', 'slavanh@eightythreecreative.com', '', '2018-12-11 14:26:56', '', 0, 'sengathit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `etc_tr_commentmeta`
--
ALTER TABLE `etc_tr_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `etc_tr_comments`
--
ALTER TABLE `etc_tr_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `etc_tr_links`
--
ALTER TABLE `etc_tr_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `etc_tr_options`
--
ALTER TABLE `etc_tr_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `etc_tr_postmeta`
--
ALTER TABLE `etc_tr_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `etc_tr_posts`
--
ALTER TABLE `etc_tr_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `etc_tr_termmeta`
--
ALTER TABLE `etc_tr_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `etc_tr_terms`
--
ALTER TABLE `etc_tr_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `etc_tr_term_relationships`
--
ALTER TABLE `etc_tr_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `etc_tr_term_taxonomy`
--
ALTER TABLE `etc_tr_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `etc_tr_usermeta`
--
ALTER TABLE `etc_tr_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `etc_tr_users`
--
ALTER TABLE `etc_tr_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `etc_tr_commentmeta`
--
ALTER TABLE `etc_tr_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `etc_tr_comments`
--
ALTER TABLE `etc_tr_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `etc_tr_links`
--
ALTER TABLE `etc_tr_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `etc_tr_options`
--
ALTER TABLE `etc_tr_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=803;

--
-- AUTO_INCREMENT for table `etc_tr_postmeta`
--
ALTER TABLE `etc_tr_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1259;

--
-- AUTO_INCREMENT for table `etc_tr_posts`
--
ALTER TABLE `etc_tr_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;

--
-- AUTO_INCREMENT for table `etc_tr_termmeta`
--
ALTER TABLE `etc_tr_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `etc_tr_terms`
--
ALTER TABLE `etc_tr_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `etc_tr_term_taxonomy`
--
ALTER TABLE `etc_tr_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `etc_tr_usermeta`
--
ALTER TABLE `etc_tr_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `etc_tr_users`
--
ALTER TABLE `etc_tr_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
