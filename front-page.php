<?php
    get_header();
    
    $projectManagers = [
        'charles monroe'    => 'charles.monroe@cityelectricsupply.com',
        'alex dixon'        => 'adixon@eightythreecreative.com',
        'lauren dilivio'    => 'ldilivio@eightythreecreative.com',
        'sengathit lavanh'    => 'slavanh@eightythreecreative.com'
    ];
?>
<div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <div class="row">
                    <article class="pb-5" id="post-<?php the_ID(); ?>" <?php post_class(); ?>
                        <header class="entry-header">
                            <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                        </header><!-- .entry-header -->
                        <div>
                            <form class="container" action="<?php echo esc_url(admin_url('admin-post.php')) ; ?>" method="post" enctype="multipart/form-data">
                                <div class="row mb-3">
                                    <div class="col-sm-6 col-12">
                                        <label for="submitterEmail">Submitter Email <span class="required">*</span></label>
                                        <input type="email" class="validate border border-secondary rounded" name="submitterEmail" placeholder="Your Email" required>
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <label for="projectManagerEmail">Send To <span class="required">*</span></label>
                                        <select name="projectManagerEmail" class="border border-secondary rounded" required>
                                            <option value="" disabled selected>Project Managers</option>
                                            <?php
                                                foreach($projectManagers as $name=>$email){
                                                    echo '<option value="' . $email . '">'. $email . '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-4 col-12">
                                        <label>Teams<span class="required">*</span></label>
                                        <select name="category" class="border border-secondary rounded" required>
                                            <option value="" disabled selected>ETC/CES/BOTH</option>
                                            <option value="7">ETC</option>
                                            <option value="1">CES</option>
                                            <option value="8">BOTH</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <label for="tags">Project of Task<span class="required">*</span></label>
                                        <select name="tags" class="border border-secondary rounded" required>
                                            <option value="" disabled selected>Project/Task</option>
                                            <option value="5">Project</option>
                                            <option value="3">Task</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <label for="priority">Priority<span class="required">*</span></label>
                                        <select name="priority" class="border border-secondary rounded" required>
                                            <option value="" disabled selected>Priority</option>
                                            <option value="11">Low</option>
                                            <option value="12">Medium</option>
                                            <option value="13">High</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <label for="title">Title <span class="required">*</span></label>
                                        <input class="validate border border-secondary rounded" type="text" name="title" placeholder="Title">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div>
                                            <h5 for="needs" class="mb-0">What do you need? <span class="required">*</span></h5>
                                            <label class="sub-detail font-italic text-secondary">Use this section to include any and all details pertaining to what needs to be done.</label>
                                        </div>
                                        <textarea class="w-100 border border-secondary rounded" name="needs" placeholder="Your Answer"></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div>
                                            <h5 for="tasks" class="mb-0">What tasks are required to complete this? And who is doing them? <span class="required">*</span></h5>
                                            <label class="sub-detail font-italic text-secondary">Please include any details regarding other tasks needed to be required. For example, if we're creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you've been given estimates or resources (i.e. specific people) for these tasks, please also include that information.</label>
                                        </div>
                                        <textarea class="w-100 border border-secondary rounded" name="tasks" placeholder="Your Answer"></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div>
                                            <h5 for="where" class="mb-0">Where should this live in TW? <span class="required">*</span></h5>
                                            <label class="sub-detail font-italic text-secondary">Please link the specific TeamWork Project name/task list (i.e. "ETC - New Hires"), if applicable.</label>
                                        </div>
                                        <select name="where" class="where border border-secondary rounded">
                                            <option value="" disabled selected>Select Project</option>
                                            <!-- Options will be loaded asynchronously -->
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <h5 for="startDate">Start Date</h5>
                                        <input id="startDate" class="border border-secondary rounded" name="startDate" placeholder="mm/dd/yyyy" required>
                                    </div>
                                    <div class="col">
                                        <h5 for="dueDate">Due Date</h5>
                                        <input id="dueDate" class="border border-secondary rounded" name="dueDate" placeholder="mm/dd/yyyy" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div>
                                            <h5 for="where" class="mb-0">Any additional details?</h5>
                                            <label for="additionalDetails" class="sub-detail font-italic text-secondary">Is there any extra context around this task you would like us to know?</label>
                                        </div>
                                        <textarea class="w-100 border border-secondary rounded" name="additionalDetails" placeholder="Your Answer"></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3 upload-container">
                                    <div class="col-12 append-file">
                                        <h5 for="upload">Uploads</h5>
                                        <input type="file" class="border border-secondary rounded" name="upload[]" placeholder="upload">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">Add Another File <i class="btn-add-file mr-3 fas fa-plus-circle"></i></div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div>
                                            <h5 class="mb-0">Will this task require any resources or involvement from the CES Team?  <span class="required">*</span></h5>
                                            <label for="resources" class="sub-detail font-italic text-secondary">If so, please provide details</label>
                                        </div>
                                        <textarea class="w-100 border border-secondary rounded" type="text" name="resources" placeholder="Your answer"></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="etc">
                                <input type="hidden" name="action" value="etc">
                                <input type="submit" class="btn btn-primary" value="Submit ETC Task">
                            </form>
                        </div>
                    </article><!-- #post-## -->
                </div>
            </div>
        </main>
    </div>
</div>
<?php wp_footer() ;?>
