<?php
global $wpdb;
$postID = $post->ID;

/*
 * Query form data from postmeta table.
 * Function declared in functions/wordpress/form-data.php.
 */
$dataMapper = queryPostMeta($wpdb,$postID);

/*
 * Get taxomonies post_tag,category,status and
 * priority from the database.
 * Function declared in functions/wordpress/form-data.php.
 */
$details = getTaxomony($postID);

$attachments = get_children(array('post_parent' => $postID));

get_header();

?>
<article class="pb-3" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content">
        <div class="container pt-5">
            <div class="card">
                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <h1 class="d-inline-block col-12 col-sm-4"><?php the_title(); ?></h1>
                            <?php if($details['status'][0]->description == 'open') { ?>
                            <button class="pmNeedsMoreInfo btn bg-success d-inline-block col-12 col-sm-3 offset-sm-1 mr-sm-2 mb-xs-2">Request More Info</button>
                            <button class="closeTask btn bg-danger d-inline-block col-12 col-sm-3">Close Task</button>
                            <?php } else { ?>
                            <h3>Task is closed</h3>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-3 text-center py-3 bg-primary text-white">Category<br><?php echo $details['category'][0]->description ; ?></div>
                        <div class="col-3 text-center py-3 bg-danger text-white">Project/Task<br><?php echo $details['post_tag'][0]->description ; ?></div>
                        <div class="col-3 text-center py-3 bg-success text-white">Status<br><?php echo $details['status'][0]->description ; ?></div>
                        <div class="col-3 text-center py-3 bg-danger text-white">Priority<br><?php echo $details['priority'][0]->description ; ?></div>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <h5 class="card-title mb-0 d-inline-block font-weight-light">Email Address : </h5> <p class="card-text d-inline-block"><?php echo $dataMapper['submitter_email'] ; ?></p>
                    </div>
                    <div>
                        <h5 class="card-title mb-0 d-inline-block font-weight-light">Project Manager Email : </h5> <p class="card-text d-inline-block"><?php echo $dataMapper['project_manager_email'] ; ?></p>
                    </div>
                    <hr>
                    <div>
                        <h5 class="card-title mb-0 font-weight-light">What do you need?</h5>
                        <p class="card-text mb-0 sub-detail text-secondary font-weight-light font-italic">With supporting text below as a natural lead-in to additional content.</p>
                        <p class="card-text pl-5 mb-0 mt-3"><?php echo $dataMapper['needs'] ; ?></p>
                    </div>
                    <hr>
                    <div>
                        <h5 class="card-title mb-0 font-weight-light">What tasks are required to complete this? And who is doing them?</h5>
                        <p class="card-text mb-0 text-secondary sub-detail font-weight-light font-italic">Please include any details regarding other tasks needed to be required. For example, if we're creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you've been given estimates or resources (i.e. specific people) for these tasks, please also include that information.</p>
                        <p class="card-text pl-5 mb-0 mt-3"><?php echo $dataMapper['tasks'] ; ?></p>
                    </div>
                    <hr>
                    <div>
                        <h5 class="card-title mb-0 font-weight-light">Where should this live in TW?</h5>
                        <p class="card-text mb-0 text-secondary sub-detail font-weight-light font-italic">Please include any details regarding other tasks needed to be required. For example, if we're creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you've been given estimates or resources (i.e. specific people) for these tasks, please also include that information.</p>
                        <p class="card-text pl-5 mb-0 mt-3"><?php echo $dataMapper['where'] ; ?></p>
                    </div>
                    <hr>
                    <div>
                        <h5 class="card-title font-weight-light">Start Date</h5>
                        <p class="card-text pl-5"><?php echo $dataMapper['start_date'] ; ?></p>
                    </div>
                    <hr>
                    <div>
                        <h5 class="card-title font-weight-light">Due Date</h5>
                        <p class="card-text pl-5"><?php echo $dataMapper['due_date'] ; ?></p>
                    </div>
                    <hr>
                    <?php if(!empty($dataMapper['additional_details'])) { ?>
                        <div>
                            <h5 class="card-title font-weight-light">Any additional details?</h5>
                            <p class="card-text pl-5"><?php echo $dataMapper['additional_details'] ; ?></p>
                        </div>
                        <hr>
                    <?php } ?>
                    <?php if(!empty($dataMapper['upload'])){ ?>
                        <div>
                            <h5 class="card-title">File upload (if needed)</h5>
                            <?php
                                foreach($attachments as $attachment){
                                    echo the_attachment_link($attachment->ID) . '<br>';
                                }
                            ?>
                        </div>
                        <hr>
                    <?php } ?>
                    <div>
                        <h5 class="card-title mb-0 font-weight-light">Will this task require any resources or involvement from the CES Team?</h5>
                        <p class="card-text mb-0 text-secondary sub-detail font-weight-light font-italic">If so, please provide details</p>
                        <p class="card-text mb-0 mt-3 pl-5"><?php echo $dataMapper['resources'] ; ?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="popup position-fixed invisible p-5 w-100 h-100">
            <div class="container h-100">
                <div class="card h-100">
                    <div class="card-body">
                        <card-title><h1 class="card-title mb-0 font-weight-light">I need more info regarding this task request.</h1></card-title>
                        <form action="<?php echo esc_url(admin_url('admin-post.php')) ; ?>" method="post">
                            <h5>title: <?php echo $post->post_title ; ?></h5>
                            <div>
                                <label for="submitterEmail">Send To:</label>
                                <input type="email" name="submitterEmail" value="<?php echo $dataMapper['submitter_email']?>" disabled>
                            </div>
                            <textarea class="col-12" name="moreInfoNeeded" placeholder="I need more info"></textarea>
                            <input type="hidden" name="taskGUID" value="<?php echo $post->guid ; ?>">
                            <input type="hidden" name="action" value="requestToSubmitter">
                            <button class="btn bg-danger text-white cancelRequest" name="cancelRequest" type="reset" value="Cancel">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Request More Info">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- CLOSE TASK DIALOGUE -->
        <div class="closeTaskForm position-fixed invisible p-5 w-100 h-100">
            <div class="container h-100">
                <div class="card h-100">
                    <div class="card-body">
                        <form action="<?php echo esc_url(admin_url('admin-post.php')) ; ?>" method="post">
                            <div class="row mb-3">
                                <div class="mr-5">
                                    <h6 class="d-inline-block">Are you sure you want to close this task?</h6>
                                    <p class="d-inline-block"><?php the_title() ; ?></p>
                                </div>
                                <div>
                                    <button class="cancel btn btn-warning">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Close Task</button>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="mr-5">
                                    <h5 class="card-title mb-0 d-inline-block font-weight-light">Submitter Address : </h5> <p class="card-text d-inline-block"><?php echo $dataMapper['submitter_email'] ; ?></p>
                                </div>
                                <div>
                                    <h5 class="card-title mb-0 d-inline-block font-weight-light">Project Manager Email : </h5> <p class="card-text d-inline-block"><?php echo $dataMapper['project_manager_email'] ; ?></p>
                                </div>
                            </div>
                            <input type="hidden" name="submitterEmail" value="<?php echo $dataMapper['submitter_email'] ; ?>">
                            <input type="hidden" name="projectManagerEmail" value="<?php echo $dataMapper['project_manager_email'] ; ?>">
                            <input type="hidden" name="postId" value="<?php echo $postID ; ?>">
                            <input type="hidden" name="taskTitle" value="<?php echo the_title() ; ?>">
                            <input type="hidden" name="url" value="<?php echo $post->guid ; ?>">
                            <input type="hidden" name="action" value="close_task">

                        </form>
                        <div class="displayContentAgain">
                            <div class="container">
                                <div class="row">
                                    <div class="col-3 text-center bg-primary text-white">Category<br><?php echo $details['category'][0]->description ; ?></div>
                                    <div class="col-3 text-center bg-danger text-white">Project/Task<br><?php echo $details['post_tag'][0]->description ; ?></div>
                                    <div class="col-3 text-center bg-success text-white">Status<br><?php echo $details['status'][0]->description ; ?></div>
                                    <div class="col-3 text-center bg-danger text-white">Priority<br><?php echo $details['priority'][0]->description ; ?></div>
                                </div>
                                <div>
                                    <h5 class="card-title mb-0 font-weight-light">What do you need?</h5>
                                    <p class="card-text mb-0 sub-detail text-secondary font-weight-light font-italic">With supporting text below as a natural lead-in to additional content.</p>
                                    <p class="card-text pl-5 mb-0"><?php echo $dataMapper['needs'] ; ?></p>
                                </div>
                                <hr class="my-1">
                                <div>
                                    <h5 class="card-title mb-0 font-weight-light">What tasks are required to complete this? And who is doing them?</h5>
                                    <p class="card-text mb-0 text-secondary sub-detail font-weight-light font-italic">Please include any details regarding other tasks needed to be required. For example, if we're creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you've been given estimates or resources (i.e. specific people) for these tasks, please also include that information.</p>
                                    <p class="card-text pl-5 mb-0"><?php echo $dataMapper['tasks'] ; ?></p>
                                </div>
                                <hr>
                                <div>
                                    <h5 class="card-title mb-0 font-weight-light">Where should this live in TW?</h5>
                                    <p class="card-text mb-0 text-secondary sub-detail font-weight-light font-italic">Please include any details regarding other tasks needed to be required. For example, if we're creating a menu for Bullion, will we be printing in house? Will they need to see the design proof for approval, etc.? If you've been given estimates or resources (i.e. specific people) for these tasks, please also include that information.</p>
                                    <p class="card-text pl-5 mb-0"><?php echo $dataMapper['where'] ; ?></p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <h5 class="card-title font-weight-light">Start Date</h5>
                                        <p class="card-text pl-5"><?php echo $dataMapper['start_date'] ; ?></p>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <h5 class="card-title font-weight-light">Due Date</h5>
                                        <p class="card-text pl-5"><?php echo $dataMapper['due_date'] ; ?></p>
                                    </div>
                                </div>
                                <hr>
                                <?php if(!empty($dataMapper['additional_details'])) { ?>
                                    <div class="row">
                                        <h5 class="card-title font-weight-light">Any additional details?</h5>
                                        <p class="card-text pl-5"><?php echo $dataMapper['additional_details'] ; ?></p>
                                    </div>
                                    <hr>
                                <?php } ?>
                                <?php if(!empty($dataMapper['upload'])){ ?>
                                    <div >
                                        <h5 class="card-title">File upload (if needed)</h5>
                                        <?php
                                        foreach($attachments as $attachment){
                                            echo the_attachment_link($attachment->ID) . '<br>';
                                        }
                                        ?>
                                    </div>
                                    <hr>
                                <?php } ?>
                                <div>
                                    <h5 class="card-title mb-0 font-weight-light">Will this task require any resources or involvement from the CES Team?</h5>
                                    <p class="card-text mb-0 text-secondary sub-detail font-weight-light font-italic">If so, please provide details</p>
                                    <p class="card-text mb-0 pl-5"><?php echo $dataMapper['resources'] ; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .entry-content -->
</article><!-- #post-## -->
<?php get_footer();
